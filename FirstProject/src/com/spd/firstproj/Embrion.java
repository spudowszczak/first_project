package com.spd.firstproj;

import java.lang.String;

public class Embrion {
	
	private int life = 100;
	private int hunger = 100;
	private String name = ""; 
	
	public  Embrion(){
	}
	public Embrion(int life){
		this.setLife(life);
	}
	public Embrion(String name,int life){
		this.setName(name);
		this.setLife(life);
	}
	
	public int getLife() {
		return life;
	}
	public void setLife(int life) {
		this.life = life;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getHunger() {
		return hunger;
	}
	public void setHunger(int hunger) {
		this.hunger = hunger;
	}
	public void feed(int foodamount) {
		if(this.hunger<=100-foodamount) {
			this.setHunger(this.hunger+foodamount);
		}else if(this.hunger+foodamount>100) {
			this.setHunger(100);
		} 
	}
	
	
	
}
